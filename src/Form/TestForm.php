<?php

/**
 * @file
 * Contains \Drupal\theme_form_test\Form\TestForm.
 */

namespace Drupal\theme_form_test\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;

/**
 * Provides a form that contains every Form API element.
 */
class TestForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'theme_form_test_form';
  }

  /**
   * Builds a form that contains every Form API element.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   *
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['checkbox'] = [
      '#type' => 'checkbox',
      '#title' => t('Checkbox'),
      '#description' => t('This is a description.')
    ];
    $form[] = [
      '#type' => 'checkboxes',
      '#options' => [t('One'), t('Two')],
      '#title' => t('Checkboxes'),
      '#title_display' => 'before',
      '#description' => t('This is a description.')
    ];
    $form[] = [
      '#type' => 'date',
      '#title' => t('Date'),
      '#default_value' => ['year' => 1991, 'month' => 8, 'day' => 27],
      '#description' => t('This is a description.')
    ];
    $form[] = [
      '#type' => 'details',
      '#title' => $this->t('Details'),
      '#open' => FALSE,
      '#description' => t('This is a description.')
    ];
    $form[] = [
      '#type' => 'fieldset',
      '#title' => t('Fieldset'),
      '#description' => t('This is a description.')
    ];
    $form[] = [
      '#type' => 'file',
      '#title' => t('File'),
      '#title_display' => 'before',
      '#size' => 22,
      '#description' => t('This is a description.')
    ];
    $form[] = [
      '#title' => t('Language'),
      '#type' => 'language_select',
      '#default_value' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
      '#languages' => LanguageInterface::STATE_ALL,
      '#description' => t('This is a description.')
    ];
    $form[] = [
      '#type' => 'machine_name',
      '#default_value' => 'default_value',
      '#maxlength' => 21,
      '#machine_name' => [
        'exists' => 'menu_edit_menu_name_exists',
      ],
      '#description' => t('This is a description.')
    ];
    $form[] = [
      '#title' => t('Managed File'),
      '#type' => 'managed_file',
      '#default_value' => '',
      '#upload_location' => 'public://image_example_images/',
      '#description' => t('This is a description.')
    ];
    $form[] = [
      '#type' => 'password',
      '#title' => t('Password'),
      '#maxlength' => 64,
      '#size' => 15,
      '#description' => t('This is a description.')
    ];
    $form[] = [
      '#type' => 'password_confirm',
      '#title' => t('Password Confirm'),
      '#size' => 25,
      '#description' => t('This is a description.')
    ];
    $form[] = [
      '#type' => 'radios',
      '#title' => t('Radios'),
      '#default_value' => 1,
      '#options' => [
        0 => t('One'),
        1 => t('Two'),
        2 => t('Three')
      ],
      '#description' => t('This is a description.')
    ];
    $form[] = [
      '#type' => 'select',
      '#title' => t('Select'),
      '#options' => [
        0 => t('One'),
        1 => t('Two'),
      ],
      '#default_value' => 0,
      '#description' => t('This is a description.')
    ];
    $form[] = [
      '#type' => 'tableselect',
      '#prefix' => t('Tableselect doesn\'t work with #title, apparently.'),
      '#header' => [
        0 => t('One'),
        1 => t('Two'),
        2 => t('Three')
      ],
      '#options' => [
        [
          0 => t('Foo'),
          1 => t('Bar'),
          2 => t('Baz')
        ],
        [
          0 => t('Qux'),
          1 => t('Xyzzy'),
          2 => t('Plugh')
        ]
      ],
      '#description' => t('This is a description.')
    ];
    $form[] = [
      '#type' => 'text_format',
      '#title' => t('Text Format'),
      '#default_value' => 'Default value',
      '#format' => NULL,
      '#description' => t('This is a description.')
    ];
    $form[] = [
      '#type' => 'textarea',
      '#title' => t('Textarea'),
      '#default_value' => 'Default value',
      '#description' => t('This is a description.')
    ];
    $form[] = [
      '#type' => 'textfield',
      '#title' => t('Textfield'),
      '#default_value' => 'Default value',
      '#size' => 60,
      '#maxlength' => 128,
      '#description' => t('This is a description.')
    ];
    $form['vertical_tabs'] = [
      '#type' => 'vertical_tabs',
      '#title' => t('Vertical Tabs'),
      '#description' => t('This is a description.')
    ];
    $form['details_1'] = [
      '#type' => 'details',
      '#title' => t('Details 1'),
      '#group' => 'vertical_tabs',
      ['#markup' => t('Details 1 Content')],
      '#description' => t('This is a description.')
    ];
    $form['details_2'] = [
      '#type' => 'details',
      '#title' => t('Details 2'),
      '#group' => 'vertical_tabs',
      ['#markup' => t('Details 2 Content')],
      '#description' => t('This is a description.')
    ];
    $form[] = [
      '#type' => 'weight',
      '#title' => t('Weight'),
      '#default_value' => 0,
      '#delta' => 10,
      '#description' => t('This is a description.')
    ];
    $form[] = [
      '#type' => 'submit',
      '#value' => t('Submit')
    ];
    $form['actions'] = [
      '#type' => 'actions'
    ];
    $form['actions']['dropbutton'] = [
      '#type' => 'dropbutton',
    ];
    $form['actions'][] = [
      '#type' => 'submit',
      '#dropbutton' => 'dropbutton',
      '#value' => t('Submit Hard')
    ];
    $form['actions'][] = [
      '#type' => 'submit',
      '#dropbutton' => 'dropbutton',
      '#value' => t('Submit Hard 2')
    ];
    $form['actions'][] = [
      '#type' => 'submit',
      '#dropbutton' => 'dropbutton',
      '#value' => t('Submit Hard: With a Vengeance')
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $form_state->setError($form['checkbox'], 'This is what an error looks like.');
    drupal_set_message('This is what a message looks like.');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}
